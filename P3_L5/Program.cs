﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RandomDataGenerator.Randomizers;
using RandomDataGenerator.FieldOptions;

namespace P3_L5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lista = Enumerable.Range(1, 100).ToList();

            #region cz1 
            /*
            

             foreach(var item in lista)
             {
               //  Console.WriteLine(item);
             }


             List<double> lista2 = Enumerable.Range(110, 100)
                 .Select(x => (double)x).ToList();

             foreach(var item in lista2)
             {
                 //Console.WriteLine(item);
             }

             IEnumerable<int> podzielnePrzez3 = lista.Where(x => x % 5 == 0);

             foreach(var item in podzielnePrzez3)
             {
            //     Console.WriteLine(item);
             }

             Console.WriteLine();
             Console.WriteLine($"srednia: {lista2.Average()}");
             Console.WriteLine($"suma: {lista2.Sum()}");*/
            #endregion

            #region cz2
            /*
            List<Osoba> osoby = lista.
                     Select(x => new Osoba
                         (
                             x.ToString(),
                             "aaa",
                             x
                         )).ToList();

           



            foreach (var osoba in osoby)
            {
           //     Console.WriteLine($"{osoba.Id}:  {osoba.Nazwisko}");
            }

                osoby.
                Select(x => x.Nazwisko).
                Distinct().ToList().
                ForEach(Console.WriteLine);

            int iloscElementowNaStronie = 15;
            int nrStrony = 0;

            List<Osoba> strony = osoby.Skip(iloscElementowNaStronie * (nrStrony)).
                 Take(iloscElementowNaStronie).ToList();

            foreach(var strona in strony)
            {
                Console.WriteLine(strona.Imie);
            }
            */
            #endregion

            var randomizerFirstName = RandomizerFactory.GetRandomizer(new FieldOptionsFirstName());
            string firstName = randomizerFirstName.Generate();

            var randomizerLastName = RandomizerFactory.GetRandomizer(new FieldOptionsFirstName());
            string lastName = randomizerFirstName.Generate();


            List<Osoba> osoby = lista.Select(x => new Osoba
                 (
                      randomizerFirstName.Generate(),
                      randomizerLastName.Generate(),
                      x
                 )).ToList();

            foreach(var osoba in osoby)
            {
                Console.WriteLine($"{osoba.Id}: {osoba.Imie} {osoba.Nazwisko}");
            }
                
        } 
    }
}