﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L5
{
    class Osoba
    {
        public string Imie, Nazwisko;
        public int Id;

        public Osoba(string imie, string nazwisko, int id)
        {
            this.Imie = imie;
            this.Nazwisko = nazwisko;
            this.Id = id;
        }

        public Osoba() { }

    }
}
